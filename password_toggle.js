(function ($) {

  /**
   * Add a "Show password" checkbox to each password field.
   */
  Drupal.behaviors.showPassword = {
    attach: function (context) {
      // Create the checkbox.
      var showPassword = $('<label class="password-toggle"><input type="checkbox" />' + Drupal.t('Show password') + '</label>');
      // Add click handler to checkboxes.
      $(':checkbox', showPassword).click(function () {
        if ($(this).is(':checked')) {
          $password_field = $(this).closest('.form-type-password').find(':password');
          $password_field.prop('type', 'text');
          $password_field.addClass('password-toggled');
        }
        else {
          $password_field = $(this).closest('.form-type-password').find('.password-toggled');
          $password_field.prop('type', 'password');
          $password_field.removeClass('password-toggled');
        }
      });

      var $passwordInput = $(context).find(':password');

      // Add checkbox to all password field on the current page.
      showPassword.insertAfter($passwordInput);
    }
  };

})(jQuery);
